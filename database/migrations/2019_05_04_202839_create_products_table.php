<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('cate_id')->unsigned();
			$table->integer('brand_id')->unsigned();
			$table->string('product_name', 32);
			$table->integer('qty');
			$table->integer('price');
			$table->integer('total_qty')->nullable();
			$table->string('status', 32)->default('active');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('products');
	}
}
