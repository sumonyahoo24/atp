<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomarsTable extends Migration {

	public function up()
	{
		Schema::create('customars', function(Blueprint $table) {
			$table->increments('id');
			$table->string('cus_name', 64);
			$table->bigInteger('phone');
			$table->string('address', 64)->nullable();
			$table->bigInteger('due')->default('0');
			$table->string('status')->default('active');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('customars');
	}
}
