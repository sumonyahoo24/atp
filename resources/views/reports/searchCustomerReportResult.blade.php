@extends('master')

@section('sidebar')

    @include('partials.sidebar')

@stop

@section('contant')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">Search Again</h1>
                        <form class="forms-sample" action="{{route('searchCustomerReportProcess')}}" role="" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="customerName">Customer Name</label>
                                <select name="customerName" class="form-control" id="customerName">
                                    <option value="">--Select Customer--</option>
                                    @foreach($cusName as $cusData)
                                        <option value="{{$cusData->id}}">{{$cusData->cus_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-success mr-2">Search</button>
                            <button class="btn btn-danger" type="reset">Reset</button>
                        </form>
                        <br>
                        <br>
                        <h1 class="card-title">Search Customer Results</h1>
                        <div class="table-responsive">
                            <table class="table table-striped table-dark">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Customar Name
                                    </th>
                                    <th>
                                        Phone
                                    </th>
                                    <th>
                                        Total Due
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($searchCustomer as $data)
                                    <tr class="">
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            {{$data->cus_name}}
                                        </td>
                                        <td>
                                            {{$data->phone}}
                                        </td>
                                        <td>
                                            {{$data->due}}
                                        </td>
                                        <td>

                                        <td>
                                            <a class="fontawesom_icon" href="{{route('confirmSellProcess', $data->id)}}">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                        </td>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
