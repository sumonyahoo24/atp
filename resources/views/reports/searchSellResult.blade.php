@extends('master')

@section('sidebar')

    @include('partials.sidebar')

@stop

@section('contant')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">Search Again</h1>
                        <form class="forms-sample" action="{{route('searchReportProcess')}}" role="" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="firstDate">Start Date</label>
                                <br>
                                <input type="date" id="firstDate" class="form-control" name="firstDate">
                            </div>
                            <div class="form-group">
                                <label for="lastDate">End Date</label>
                                <br>
                                <input type="date" id="lastDate" class="form-control" name="lastDate">
                            </div>
                            <button type="submit" class="btn btn-success mr-2">Search Again</button>
                            <button class="btn btn-danger" type="reset">Reset</button>
                        </form>
                        <br>
                        <br>
                        <h1 class="card-title">Search Sell Results</h1>
                        <div class="table-responsive">
                            <table class="table table-striped table-dark">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Customar Name
                                    </th>
                                    <th>
                                        Brand Name
                                    </th>
                                    <th>
                                        Category Name
                                    </th>
                                    <th>
                                        Product Name
                                    </th>
                                    <th>
                                        Quantity
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                    <th>
                                        Total Price
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($searchSell as $data)
                                    <tr class="">
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            {{$data->sellCustomar->cus_name}}
                                        </td>
                                        <td>
                                            {{$data->sellProduct->productBrand->brand_name}}
                                        </td>
                                        <td>
                                            {{$data->sellProduct->productCate->cate_name}}
                                        </td>
                                        <td>
                                            {{$data->sellProduct->product_name}}
                                        </td>
                                        <td>
                                            {{$data->qty}}
                                        </td>
                                        <td>
                                            {{$data->price}}
                                        </td>
                                        <td>
                                            {{$data->total_qty}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
