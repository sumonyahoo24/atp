@extends('master')

@section('sidebar')

    @include('partials.sidebar')

@stop

@section('contant')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
                <div class="row flex-grow">
                    <div class="col-12">
                        <div class="card">
                            @if(Session::has('message'))
                                <p class="alert alert-success">{{ Session::get('message') }}</p>
                            @endif
                            <div class="card-body">
                                <h1 class="card-title">Add Product</h1>
                                <form class="forms-sample" action="{{route('addProductProcess')}}" role="" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="productName">Product Name</label>
                                        <input type="text" name="productName" class="form-control" id="productName" placeholder="Enter Product Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="select">Band Name</label>
                                        <select class="form-control" id="bandName" name="bandName">
                                            <option value="">--Select Brand Name--</option>
                                            @foreach($bandName as $bandNames)
                                                <option value="{{$bandNames->id}}">{{$bandNames->brand_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="select">Category Name</label>
                                        <select class="form-control" id="categoryName" name="categoryName">
                                            <option value="">--Select Category Name--</option>
                                            @foreach($cateName as $cateNames)
                                                <option value="{{$cateNames->id}}">{{$cateNames->cate_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="brandName">Quantity</label>
                                        <input type="text" name="quantity" class="form-control" id="quantity" placeholder="Enter Quantity">
                                    </div>
                                    <div class="form-group">
                                        <label for="price">Price</label>
                                        <input type="text" name="price" class="form-control" id="price" placeholder="Enter Price">
                                    </div>
                                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                                    <button class="btn btn-danger" type="reset">Reset</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
