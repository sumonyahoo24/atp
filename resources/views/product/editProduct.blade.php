@extends('master')

@section('sidebar')

    @include('partials.sidebar')

@stop

@section('contant')



        <div class="row">

            <div class="col-md-12 d-flex align-items-stretch grid-margin">
                <div class="row flex-grow">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h1 class="card-title">Edit Product</h1>
                                <form class="forms-sample" action="{{route('editProductProcess', $product_info->id)}}" method="POST"
                                      role="form">
                                    @method('put')
                                    @csrf
                                    <div class="form-group">
                                        <label for="productName">Product Name</label>
                                        <input type="text" name="product_name" class="form-control"
                                               value="{{$product_info->product_name}}" id="productName"
                                               placeholder="Enter Product Name">

                                        <label for="Quantity">Quantity</label>
                                        <input type="text" name="quantity" class="form-control"
                                               value="{{$product_info->qty}}" id="Quantity"
                                               placeholder="Enter Quantity ">

                                        <label for="productPrice">Product Price</label>
                                        <input type="text" name="price" class="form-control"
                                               value="{{$product_info->price}}" id="productPrice"
                                               placeholder="Enter Product Price ">
                                    </div>
                                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                                    <button class="btn btn-danger" type="reset">Reset</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
