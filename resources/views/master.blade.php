<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ATP</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/vendor.bundle.addons.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('fontawesome/css/all.min.css')}}">
    <!-- endinject -->
    <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>


<!--===========Header Start Here===========-->
@include('partials.header')
<!--===========Header End Here===========-->
<!--===========sidebar Start Here===========-->

@yield('sidebar')

<!--=============sidebar End Here=============-->
<!--=============Page_Contant Start Here=============-->
@yield('contant')
<!--=============Page_Contant End Here=============-->
<!--=============Footer Start Here=============-->
@include('partials.footer')
<!--=============Footer End Here=============-->



{{-- Jquery --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js  "></script>
<!-- plugins:js -->
<script src="vendors/js/vendor.bundle.base.js"></script>
<script src="vendors/js/vendor.bundle.addons.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{asset('js/off-canvas.js')}}"></script>
<script src="{{asset('js/misc.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('js/jquery.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('fontawesome/js/all.min.js')}}"></script>
<link rel="stylesheet" href="{{ asset('css/fontawesome/css/all.css')}}">
<script src="{{asset('js/plugins.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- End custom js for this page-->

<script type="text/javascript">
    //console.log("hmm its change");
    $(document).ready(function(){
        //Category Name Change Start
        $
        (document).on('change','#sellbrandName',function(){
            var brandId=$(this).val();
            var div=$(this).parent();
            var op=" ";

            $.ajax({
                type:'get',
                url:'{!!URL::to('jsonAddSell')!!}',
                data:{'id':brandId},
                success:function(data){
                   if(data.length>0)
                   {
                       //console.log(data);
                    for(var i=0;i<data.length;i++){
                        // op+='<option value="'+data[i].id+'">'+data[i].cate_name+'</option>';
                        $("#sellcateName").append('<option value="'+data[i].id+'">'+data[i].cate_name+'</option>');
                    }
                   }else {
                       var select = document.getElementById("sellcateName");
                       select.options.length=0;
                   }
                },
                error:function(){
                }
            });
        });
        //Category Name Change End
        //Product Name Change Start
        $
        (document).on('change','#sellcateName',function(){
            //console.log('its changing...');
            var cateId=$(this).val();
            //console.log(brandId);
            var div=$(this).parent();
            //console.log(div);
            var op=" ";

            $.ajax({
                type:'get',
                url:'{!!URL::to('addSellProduct')!!}',
                data:{'id':cateId},
                success:function(data){

                   if(data.length>0) {
                       let select = document.getElementById("sellproductName");
                       select.options.length=0;
                    for (let i=0;i<data.length;i++) {
                        var proname= $("#sellproductName");
                        proname.append('<option value="'+data[i].id+'">'+data[i].product_name+'</option>');
                    }

                   }else {
                       let select = document.getElementById("sellproductName");
                       select.options.length=0;
                   }
                },
                error:function(){
                }
            });
        });
        //Product Name Change End
        //Price Name Change Start
        $
        (document).on('change','#sellproductName',function(){
            var productId=$(this).val();
            var div=$(this).parent();
            var op=" ";

            $.ajax({
                type:'get',
                url:'{!!URL::to('addSellPrice')!!}',
                data:{'id':productId},
                success:function(data){
                    console.log(data);
                    document.getElementById("sellprice").value=data.price;
                    document.getElementById("sellMaxQty").value='( You Can Add Max '+data.qty+' )';
                },
                error:function(){
                }
            });
        });
        //Price Name Change End
    });
</script>

</body>

</html>
