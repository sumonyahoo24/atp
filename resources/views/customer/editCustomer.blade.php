@extends('master')

@section('sidebar')

    @include('partials.sidebar')

@stop

@section('contant')



        <div class="row">

            <div class="col-md-12 d-flex align-items-stretch grid-margin">
                <div class="row flex-grow">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h1 class="card-title">Edit Customer</h1>
                                <form class="forms-sample" action="{{route('editCustomerProcess', $customer_info->id)}}" method="POST"
                                      role="form">
                                    @method('put')
                                    @csrf
                                    <div class="form-group">
                                        <label for="customerName">Customer Name</label>
                                        <input type="text" name="customer_name" class="form-control"
                                               value="{{$customer_info->cus_name}}" id="customerName"
                                               placeholder="Enter Customer Name">

                                        <label for="customerPhone">Customer Phone No</label>
                                        <input type="text" name="phone" class="form-control"
                                               value="{{$customer_info->phone}}" id="customerPhone"
                                               placeholder="Enter Customer Name">

                                        <label for="customerPhone">Customer Address</label>
                                        <input type="text" name="address" class="form-control"
                                               value="{{$customer_info->address}}" id="customer_address"
                                               placeholder="Enter Customer Address">
                                    </div>
                                    <button type="submit" class="btn btn-success mr-2">Submit</button>
                                    <button class="btn btn-danger" type="reset">Reset</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
