@extends('master')

@section('sidebar')

    @include('partials.sidebar')

@stop

@section('contant')

    <div class="content-wrapper">

        <div class="row">
            <div class="col-lg-12 stretch-card">
                <div class="card">
                    @if(Session::has('message'))
                        <p class="alert alert-success">{{ Session::get('message') }}</p>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card-body">
                        <h1 class="card-title">Category List</h1>
                        <div class="table-responsive">
                            <table class="table table-striped table-dark">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Category Name
                                    </th>
                                    <th>
                                        Brand Name
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categoryNames as $categoryName)
                                    <tr class="">
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            {{$categoryName->cate_name}}
                                        </td>
                                        @foreach(($categoryName->brandCategory) as $brandName)
                                            <td>
                                                {{$brandName->brand_name}}
                                            </td>
                                        @endforeach
                                        <td>
                                            {{$categoryName->status}}
                                        </td>


                                        <!-- Button trigger modal -->
                                        <td>
                                            <a class="btn btn-success"
                                               href="{{route('updateCategory',$categoryName->id)}}"><i
                                                        class="fa fa-edit"></i></a>

{{--                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">--}}
{{--                                                Edit1--}}
{{--                                            </button>--}}
                                            <!-- Modal -->
                                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title edit_modal_title" id="exampleModalLabel">Edit Brand</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form class="" action="{{route('editBrandProcess', $categoryName->id)}}" role="" method="post" enctype="multipart/form-data">
                                                                @csrf
                                                                <div class="form-group">
                                                                    <label for="brandName">Category Name</label>
                                                                    <input type="text" name="brandName" class="form-control" id="brandName" value="{{$categoryName->id}}">
                                                                </div>
                                                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                                                <button class="btn btn-danger" type="reset">Reset</button>
                                                            </form>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($categoryName->status=="active")
                                                <a href="{{route('inactiveCategory', $categoryName->id)}}" class="btn btn-info">
                                                    Inactive
                                                </a>
                                            @else
                                                <a href="{{route('activeCategory', $categoryName->id)}}" class="btn btn-info">
                                                    Active
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
