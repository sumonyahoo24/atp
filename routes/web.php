<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'Controller@index')->name('index');
Route::get('menuItem', 'Controller@menuItem')->name('menuItem');
Route::get('brandList', 'BrandController@brandList')->name('brandList');
Route::get('inactiveBrandList', 'BrandController@inactiveBrandList')->name('inactiveBrandList');
Route::get('activeBrandList', 'BrandController@activeBrandList')->name('activeBrandList');
Route::get('addBrand', 'BrandController@addBrand')->name('addBrand');
Route::post('addBrandProcess', 'BrandController@addbrandProcess')->name('addBrandProcess');
Route::get('updateBand/{id}', 'BrandController@updateBand')->name('updateBand');
Route::put('editBrandProcess/{id}', 'BrandController@editBrandProcess')->name('editBrandProcess');
Route::get('inactiveBrand/{id}', 'BrandController@inactiveBrand')->name('inactiveBrand');
Route::get('activeBrand/{id}', 'BrandController@activeBrand')->name('activeBrand');

// Category Routs

Route::get('categoryList', 'CategoryController@categoryList')->name('categoryList');
Route::get('inactiveCategoryList', 'CategoryController@inactiveCategoryList')->name('inactiveCategoryList');
Route::get('activeCategoryList', 'CategoryController@activeCategoryList')->name('activeCategoryList');
Route::get('addCategory', 'CategoryController@addCategory')->name('addCategory');
Route::post('addCategoryProcess', 'CategoryController@addCategoryProcess')->name('addCategoryProcess');
Route::get('updateCategory/{id}', 'CategoryController@updateCategory')->name('updateCategory');
Route::put('editCategoryProcess/{id}', 'CategoryController@editCategoryProcess')->name('editCategoryProcess');
Route::get('inactiveCategory/{id}', 'CategoryController@inactiveCategory')->name('inactiveCategory');
Route::get('activeCategory/{id}', 'CategoryController@activeCategory')->name('activeCategory');

// Product Routes

Route::get('addProduct', 'ProductController@addProduct')->name('addProduct');
Route::post('addProductProcess', 'ProductController@addProductProcess')->name('addProductProcess');
Route::get('productList', 'ProductController@productList')->name('productList');
Route::get('updateProduct/{id}', 'ProductController@updateProduct')->name('updateProduct');
Route::put('editProductProcess/{id}', 'ProductController@editProductProcess')->name('editProductProcess');

//Route::post('editProductProcess', 'ProductController@editProductProcess')->name('editProductProcess');
Route::get('inactiveProduct/{id}', 'ProductController@inactiveProduct')->name('inactiveProduct');
Route::get('activeProduct/{id}', 'ProductController@inactiveProduct')->name('activeProduct');
Route::get('inactiveProductList', 'ProductController@inactiveProductList')->name('inactiveProductList');
Route::get('activeProductList', 'ProductController@activeProductList')->name('activeProductList');
Route::get('activeProduct/{id}', 'ProductController@inactiveProduct')->name('activeProduct');
Route::get('inactiveProductList', 'ProductController@inactiveProductList')->name('inactiveProductList');





// Customer Routes

Route::get('customerList', 'CustomarController@customerList')->name('customerList');
Route::get('addCustomer', 'CustomarController@addCustomer')->name('addCustomer');
Route::post('addCustomerProcess', 'CustomarController@addCustomerProcess')->name('addCustomerProcess');
Route::get('inactiveCustomerList', 'CustomarController@inactiveCustomerList')->name('inactiveCustomerList');
Route::get('activeCustomerList', 'CustomarController@activeCustomerList')->name('activeCustomerList');
Route::get('updateCustomer/{id}', 'CustomarController@updateCustomer')->name('updateCustomer');
Route::put('editCustomerProcess/{id}', 'CustomarController@editCustomerProcess')->name('editCustomerProcess');

//Route::post('editCustomerProcess', 'CustomarController@editCustomerProcess')->name('editCustomerProcess');
Route::get('inactiveCustomer/{id}', 'CustomarController@inactiveCustomer')->name('inactiveCustomer');
Route::get('activeCustomer/{id}', 'CustomarController@activeCustomer')->name('activeCustomer');


// Customer Routes

Route::get('addSell', 'SaleController@addSell')->name('addSell');
Route::get('/jsonAddSell','SaleController@jsonAddSell');
Route::get('/addSellProduct','SaleController@addSellProduct');
Route::get('/addSellPrice','SaleController@addSellPrice');
Route::post('addSellProcess', 'SaleController@addSellProcess')->name('addSellProcess');
Route::get('confirmSell', 'SaleController@confirmSell')->name('confirmSell');
Route::get('confirmSellProcess/{id}', 'SaleController@confirmSellProcess')->name('confirmSellProcess');

//Reports

Route::get('todayDaySell', 'ReportController@todayDaySell')->name('todayDaySell');
Route::get('searchSell', 'ReportController@searchSell')->name('searchSell');
Route::post('searchReportProcess', 'ReportController@searchReportProcess')->name('searchReportProcess');
Route::get('searchCustomerReport', 'ReportController@searchCustomerReport')->name('searchCustomerReport');
Route::post('searchCustomerReportProcess', 'ReportController@searchCustomerReportProcess')->name('searchCustomerReportProcess');



