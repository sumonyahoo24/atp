<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model 
{

    protected $table = 'sales';
    public $timestamps = true;
    protected $fillable = array('cus_id', 'brand_id', 'prod_id', 'cate_id', 'qty', 'price', 'total_price');

    public function sellCustomar()
    {
        return $this->hasOne(Customar::Class, 'id', 'cus_id');
    }
    public function sellProduct()
    {
        return $this->hasOne(Product::Class, 'id', 'prod_id');
    }

}
