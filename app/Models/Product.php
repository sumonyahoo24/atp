<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model 
{

//    protected $table = 'products';
//    public $timestamps = true;
//protected $fillable = array('cate_id', 'brand_id', 'product_name', 'qty', 'price', 'total_qty', 'status');

    protected $guarded = [];
    //public function productCate()
    //{
    //    return $this->hasOne('Category', 'id');
    //}
    public  function productCate()
    {
        return $this->hasOne(Category::Class, 'id', 'cate_id');
    }
    public function productBrand()
    {
        return $this->hasOne(Brand::Class, 'id', 'brand_id');
    }

}
